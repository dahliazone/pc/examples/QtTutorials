#ifndef GOTOCELLDIALOG_H
#define GOTOCELLDIALOG_H

//#include <QMainWindow>
#include <QDialog>

/*namespace Ui {
class GoToCellDialog;
}*/

#include "ui_gotocelldialog.h"

class GoToCellDialog : public QDialog, public Ui::GoToCellDialog //QMainWindow
{
	Q_OBJECT

public:
	GoToCellDialog(QWidget *parent = 0);
	//~GoToCellDialog();

private slots:
	void on_lineEdit_textChanged();

private:
	//Ui::GoToCellDialog *ui;
};

#endif // GOTOCELLDIALOG_H
