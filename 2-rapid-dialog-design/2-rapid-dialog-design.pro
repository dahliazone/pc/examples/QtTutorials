#-------------------------------------------------
#
# Project created by QtCreator 2014-06-19T01:34:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2-rapid-dialog-design
TEMPLATE = app


SOURCES += main.cpp\
        gotocelldialog.cpp

HEADERS  += gotocelldialog.h

FORMS    += gotocelldialog.ui
