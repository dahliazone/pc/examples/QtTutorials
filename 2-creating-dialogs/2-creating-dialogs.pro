#-------------------------------------------------
#
# Project created by QtCreator 2014-06-18T22:42:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2-creating-dialogs
TEMPLATE = app


SOURCES += main.cpp\
    FindDialog.cpp

HEADERS  += \
    FindDialog.hpp

FORMS    +=
