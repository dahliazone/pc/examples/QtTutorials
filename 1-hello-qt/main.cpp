#include "mainwindow.h"
#include <QApplication>

//#include <QLabel>

//#include <QPushButton>

#include <QHBoxLayout>
#include <QSlider>
#include <QSpinBox>


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	//MainWindow w;
	//w.show();

	// Simple label
	//QLabel *label = new QLabel("Hello Qt");
	//label->show();

	// Quit button
	//QPushButton *button = new QPushButton("Quit");
	//QObject::connect(button, SIGNAL(clicked()),
	//				 &app, SLOT(quit()));
	//button->show();

	// Enter your age
	QWidget *window = new QWidget;
	window->setWindowTitle("Enter your age");

	QSpinBox *spinBox = new QSpinBox;
	QSlider *slider = new QSlider(Qt::Horizontal);
	spinBox->setRange(0, 130);
	slider->setRange(0, 130);

	QObject::connect(spinBox, SIGNAL(valueChanged(int)),
					 slider, SLOT(setValue(int)));
	QObject::connect(slider, SIGNAL(valueChanged(int)),
					 spinBox, SLOT(setValue(int)));
	spinBox->setValue(35);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(spinBox);
	layout->addWidget(slider);
	window->setLayout(layout);

	window->show();

	return app.exec();
}
